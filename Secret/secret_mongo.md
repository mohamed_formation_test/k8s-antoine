# Création et utilisation d'un Secret

## Exercice

Dans cet exercice vous allez utiliser un Secret pour vous connecter à une base de données externe.

### 1. Le context

L'image *registry.gitlab.com/lucj/messages:v1.0.4* contient une application simple qui écoute sur le port 3000 et permet, via des requêtes HTTP, de créer des messages ou de lister les messages existants dans une base de données *MongoDB*. L'URL de connexion de cette base doit être fournie à l'application de façon à ce que celle-ci puisse s'y connecter. Nous pouvons lui fournir via une variable d'environnement MONGODB_URL ou via un fichier qui devra être accessible depuis */app/db/mongodb_url*.

### 2. La base de données

Pour cet exercice la base de données suivante sera utilisée:

- host: *db.techwhale.io*
- port: *27017* (port par défaut de Mongo)
- nom de la base: *message*
- le tls est activé mais utilise un certificat autosigné
- utilisateur: *k8sExercice* / *k8sExercice*

L'URL de connection est la suivante:

```
mongodb://k8sExercice:k8sExercice@db.techwhale.io:27017/message?ssl=true&tlsInsecure=true&authSource=admin
```

### 3. Création du Secret

Créez un Secret nommé *mongo*, le champ *data* de celui-ci doit contenir la clé *mongo_url* dont la valeur est la chaine de connection spécifiée ci-dessus.

Choisissez pour cela l'une des options suivantes:

- Option 1: utilisation de la commande `kubectl create secret generic` avec l'option `--from-file`

- Option 2: utilisation de la commande `kubectl create secret generic` avec l'option `--from-literal`

- Option 3: utilisation d'un fichier de spécification

### 4. Utilisation du Secret dans une variable d'environnement

Définissez un Pod nommé *messages-env* dont l'unique container a la spécification suivante:

- image: *registry.gitlab.com/lucj/messages:v1.0.4*
- une variable d'environnement *MONGODB_URL* ayant la valeur liée à la clé *mongo_url* du Secret *mongo* créé précédemment

Créez ensuite ce Pod et exposez le en utilisant la commande `kubectl port-forward` en faisant en sorte que le port 3000 de votre machine locale soit mappé sur le port 3000 du Pod *messages-env*.

Depuis un autre terminal, vérifiez que vous pouvez créer un message avec la commande suivante:

Note: assurez vous de remplacer *YOUR_NAME* par votre prénom

```
curl -H 'Content-Type: application/json' -XPOST -d '{"msg":"hello from YOUR_NAME"}' http://localhost:3000/messages
```

### 5. Utilisation du Secret dans un volume

Définissez un Pod nommé *messages-vol* ayant la spécification suivante:

- un volume nommé *mongo-creds* basé sur le Secret *mongo*
- un container ayant la spécification suivante:
  - image: *registry.gitlab.com/lucj/messages:v1.0.4*
  - une instructions *volumeMounts* permettant de monter la clé *mongo_url* du volume *mongo*mongo-creds* dans le fichier */app/db/mongo_url*

Créez le Pod et vérifier que vous pouvez créer un message de la même façon que dans le point précédent en exposant le Pod via un *port-forward*.

---

## Correction

### 3. Création du Secret

- Option 1: utilisation de la commande `kubectl create secret generic` avec l'option `--from-file`

Utilisez la commande suivante afin de créer un fichier *mongo_url* contenant la chaine de connexion à la base de données:

```
echo -n "mongodb://k8sExercice:k8sExercice@db.techwhale.io:27017/message?ssl=true&tlsInsecure=true&authSource=admin" > mongo_url
```

Nous crééons ensuite le Secret à partir de ce fichier:

```
kubectl create secret generic mongo --from-file=mongo_url
```

- Option 2: utilisation de la commande `kubectl create secret generic` avec l'option `--from-literal`

La commande suivante permet de créer le Secret à partir de valeurs littérales

```
kubectl create secret generic mongo \
--from-literal=mongo_url='mongodb://k8sExercice:k8sExercice@db.techwhale.io:27017/message?ssl=true&tlsInsecure=true&authSource=admin'
```

- Option 3: utilisation d'un fichier de spécification

La première étape est d'encrypter en base64 la chaine de connexion

```
$ echo -n 'mongodb://k8sExercice:k8sExercice@db.techwhale.io:27017/message?ssl=true&tlsInsecure=true&authSource=admin' | base64

bW9uZ29kYjovL2s4c0V4ZXJjaWN...mYXV0aFNvdXJjZT1hZG1pbg==
```

Ensuite nous pouvons définir le fichier de spécification mongo-secret.yaml:

```
apiVersion: v1
kind: Secret
metadata:
  name: mongo
data:
  mongo_url: bW9uZ29kYjovL2s4c0V4ZXJjaWN...mYXV0aFNvdXJjZT1hZG1pbg==
```

La dernière étape consiste à créer le Secret à partir de ce fichier

```
kubectl apply -f mongo-secret.yaml
```

### 4. Utilisation du Secret dans une variable d'environnement

Nous définissons la spécification suivante dans le fichier *messages-env.yaml*

```
apiVersion: v1
kind: Pod
metadata:
  name: messages-env
spec:
  containers:
  - name: messages
    image: registry.gitlab.com/lucj/messages:v1.0.4
    env:
    - name: MONGODB_URL
      valueFrom:
        secretKeyRef:
          name: mongo
          key: mongo_url
```

Nous pouvons alors créer le Pod:

```
kubectl apply -f messages-env.yaml
```

La commande suivante permet d'exposer en localhost l'API tournant dans le container du Pod:

```
kubectl port-forward messages-env 3000:3000
```

Depuis un autre terminal sur notre machine locale, nous pouvons alors envoyer une requête POST sur l'API:

Note: assurez vous de remplacer *YOUR_NAME* par votre prénom

```
curl -H 'Content-Type: application/json' -XPOST -d '{"msg":"hello from USER_NAME"}' http://localhost:3000/messages
```

La réponse retournée est similaire à celle ci-dessous:
```
{"msg":"hello from USER_NAME","created_at":"2022-12-31T18:04:03.581Z"}
```

Nous pouvons ensuite arrêter le port-forward.

### 5. Utilisation du Secret dans un volume

Nous définissons la spécification suivante dans le fichier *messages-vol.yaml*

```
apiVersion: v1
kind: Pod
metadata:
  name: messages-vol
spec:
  containers:
  - name: messages
    image: registry.gitlab.com/lucj/messages:v1.0.4
    volumeMounts:
    - name: mongo-creds
      mountPath: "/app/db"
      readOnly: true
  volumes:
  - name: mongo-creds
    secret:
      secretName: mongo
```

Nous pouvons alors créer le Pod:

```
kubectl create -f messages-vol.yaml
```

La commande suivante permet d'exposer en localhost l'API tournant dans le container du Pod:

```
kubectl port-forward messages-vol 3000:3000
```

Depuis la machine locale, nous pouvons alors envoyer une requête POST sur l'API:

```
curl -H 'Content-Type: application/json' -XPOST -d '{"msg":"hello from USER_NAME"}' http://localhost:3000/messages
```

Nous obtenons alors une réponse simimaire à la suivante:

```
{"msg":"hello from USER_NAME","created_at":"2023-01-01T10:51:43.900Z"}
```
